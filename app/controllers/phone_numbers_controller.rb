# == Schema Information
#
# Table name: phone_numbers
#
#  id             :integer          not null, primary key
#  number         :string
#  phone_type_id  :integer
#  phoneable_id   :integer
#  phoneable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class PhoneNumbersController < ApplicationController
  before_filter :load_phoneable
  before_action :set_phone_number, only: [:edit, :update, :destroy]
  
  def index
    @phone_numbers = @phoneable.phones_numbers
  end

  def new
    @phone_number = @phoneable.phone_numbers.new
  end
  
  def edit
    # @phone_number = @phoneable.phone_numbers.new
  end

  def create
    @phone_number = @phoneable.phone_numbers.new(phone_number_params)
    if @phone_number.save
      redirect_to @phoneable, notice: "Phone number created."
    else
      render :new
    end
  end
  
  def update
    @contact = @phoneable
    respond_to do |format|
      if @phone_number.update(phone_number_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

private

  def set_phone_number
    @phone_number = PhoneNumber.find(params[:id])
  end

  def load_phoneable
    resource, id = request.path.split('/')[1, 2]
    @phoneable = resource.singularize.classify.constantize.find(id)
  end
  
  def phone_number_params
    params.require(:phone_number).permit(:number)
  end

end
