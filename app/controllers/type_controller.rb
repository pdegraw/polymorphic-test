class TypeController < ApplicationController
  before_filter :load_typeable
  
  def index
    @types = @typeable.types
  end

  def new
    @type = @typeable.types.new
  end

  def create
    @type = @typeable.types.new(type_params)
    if @type.save
      redirect_to @typeable, notice: "Type created."
    else
      render :new
    end
  end

private

  def load_typeable
    resource, id = request.path.split('/')[1, 2]
    @typeable = resource.singularize.classify.constantize.find(id)
  end
  
  def type_params
    params.require(:type).permit(:title)
  end

end