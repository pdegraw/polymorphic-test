module ApplicationHelper
  
  def page_header_index(name)
    simple_format(name, {class: 'page-header'}, wrapper_tag: "h1")
  end
  
end