# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module ContactsHelper
  
  def page_header(name)
    simple_format(name.name, {class: 'page-header'}, wrapper_tag: "h1")
  end
  
end
