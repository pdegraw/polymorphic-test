# == Schema Information
#
# Table name: jobs
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  job_type   :string
#

module JobsHelper

  def job_type_options
    [["Pick Up Rental", "PUR"], ["Service Rental", "SR"], ["Repair", "RPR"], ["Event Management Services", "EMS"]]
  end

end
