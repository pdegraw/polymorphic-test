# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Contact < ActiveRecord::Base
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :phone_numbers, as: :phoneable, dependent: :destroy
end
