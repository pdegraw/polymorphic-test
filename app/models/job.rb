# == Schema Information
#
# Table name: jobs
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  job_type   :string
#

class Job < ActiveRecord::Base
  JOB_TYPES = ["PUR-Pick Up Rental", "SR-Service Rental", "RPR-Repair", "EMS-Event Management Services"]
  
  has_many :comments, as: :commentable, dependent: :destroy
  has_one :type, as: :typeable
  # belongs_to :true, polymorphic: true
end
