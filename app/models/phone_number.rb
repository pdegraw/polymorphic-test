# == Schema Information
#
# Table name: phone_numbers
#
#  id             :integer          not null, primary key
#  number         :string
#  phone_type_id  :integer
#  phoneable_id   :integer
#  phoneable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class PhoneNumber < ActiveRecord::Base
  belongs_to :phone_type
  belongs_to :phoneable, polymorphic: true
end
