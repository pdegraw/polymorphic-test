# == Schema Information
#
# Table name: types
#
#  id            :integer          not null, primary key
#  title         :string
#  typeable_id   :integer
#  typeable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Type < ActiveRecord::Base
  belongs_to :typeable, polymorphic: true
  # has_many :jobs, as: :typeable
end
