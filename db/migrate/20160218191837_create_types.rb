class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
      t.string :title
      t.integer :typeable_id
      t.string :typeable_type

      t.timestamps null: false
    end
  end
end
