class RemoveTypeFromPhoneType < ActiveRecord::Migration
  def change
    remove_column :phone_types, :type, :string
  end
end
