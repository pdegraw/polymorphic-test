class AddTitleToPhoneTypes < ActiveRecord::Migration
  def change
    add_column :phone_types, :title, :string
  end
end
