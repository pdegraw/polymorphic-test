class RenamePhoneToPhoneNumber < ActiveRecord::Migration
  def change
     rename_table :phones, :phone_number
   end 
end
