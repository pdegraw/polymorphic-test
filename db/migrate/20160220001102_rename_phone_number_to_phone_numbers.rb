class RenamePhoneNumberToPhoneNumbers < ActiveRecord::Migration
  def change
    rename_table :phone_number, :phone_numbers
  end
end
